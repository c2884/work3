import unittest
import socket
import struct
import base64
from unittest.mock import patch, MagicMock

import my_udp_server

class TestMyUdpServer(unittest.TestCase):
    
    @patch('socket.socket')
    def test_receive_packet(self, mock_socket):
        # Set up mock socket to return a packet
        expected_packet = struct.pack('!HHHH', 1234, 5678, 16, 0, b'test payload')
        mock_socket.return_value.recvfrom.return_value = (expected_packet, ('1.2.3.4', 5678))
        
        # Call the function to receive the packet
        packet = my_udp_server.receive_packet(mock_socket.return_value)
        
        # Assert that the returned packet matches the expected packet
        self.assertEqual(packet, expected_packet)
        
    def test_decode_packet(self):
        # Set up a packet to decode
        packet_data = struct.pack('!HHHH', 1234, 5678, 16, 0, b'test payload')
        
        # Call the function to decode the packet
        decoded_packet = my_udp_server.decode_packet(packet_data)
        
        # Assert that the decoded packet matches the expected values
        expected_values = (1234, 5678, 16, 0, 'test payload')
        self.assertEqual(decoded_packet, expected_values)
        
    @patch('my_udp_server.decode_packet')
    def test_print_packet_info(self, mock_decode_packet):
        # Set up mock decoded packet to return expected values
        mock_decoded_packet = (1234, 5678, 16, 0, 'test payload')
        mock_decode_packet.return_value = mock_decoded_packet
        
        # Call the function to print packet info
        my_udp_server.print_packet_info(b'test packet')
        
        # Assert that the expected values were printed
        expected_output = f"Base64: b'dGVzdCBwYXNrZXQ=\\n'\nServer Sent: b'test packet'\nDecoded Packet:\nSource Port: 1234\nDest Port: 5678\nData Length: 16\nChecksum: 0\nPayload: test payload\n"
        self.assertEqual(mock_stdout.getvalue(), expected_output)
        
        
        #The above tests assume that the functions receive_packet and decode_packet are 
        # defined in the my_udp_server module. If these functions are defined within the 
        # same script as the main function, then you can replace my_udp_server with the name 
        # of the script file (without the .py extension) in the patch statements.
