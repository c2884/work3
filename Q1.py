import socket
import struct
import base64

# Create a UDP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# Bind the socket to a local address and port
sock.bind(('0.0.0.0', 1234))

# Receive the packet from the server
data, addr = sock.recvfrom(1024)

# Decode the packet
decoded_packet = struct.unpack('!HHHH', data[:8]) + (data[8:],)
source_port, dest_port, length, checksum, payload = decoded_packet

# Print the packet information
print('Base64:', base64.b64encode(data))
print('Server Sent:', data)
print('Decoded Packet:')
print('Source Port:', source_port)
print('Dest Port:', dest_port)
print('Data Length:', length)
print('Checksum:', checksum)
print('Payload:', payload.decode())