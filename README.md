
# Worksheet 3 

A brief description of what this project does and who it's for

This Python script demonstrates how to receive and decode UDP packets in Python. It includes the following three sections:

UDP Packet Receiver: This section sets up a UDP socket and receives a packet from a server. It then decodes the packet and prints its information.

Checksum Calculator: This section defines a function that calculates the checksum of a given UDP packet. It takes in the source port, destination port, length, and payload of the packet and returns the corresponding checksum.

UDP Packet Sender and Receiver: This section uses the previous sections to send and receive UDP packets asynchronously. It sends a packet with payload '1111' to a server every second and decodes the response. It also verifies the checksum of the received packet.

To run the script, simply execute the main() function. Note that you may need to modify the IP address and port number to match your server configuration.