def calculate_checksum(source_port, dest_port, length, payload):
    # Initialize the checksum to 0
    checksum = 0

    # Add the source port to the checksum
    checksum += source_port

    # Add the destination port to the checksum
    checksum += dest_port

    # Add the length to the checksum
    checksum += length

    # Combine the payload bytes into 16-bit words and add them to the checksum
    i = 0
    while i < len(payload):
        if i + 1 < len(payload):
            word = (payload[i] << 8) + payload[i + 1]
        else:
            word = (payload[i] << 8)
        checksum += word
        i += 2

    # Add the carry bits back to the checksum
    checksum = (checksum & 0xffff) + (checksum >> 16)

    # Take the one's complement of the checksum
    checksum = ~checksum & 0xffff

    return checksum


Assuming you have a packet consisting of a source port, destination port, length, and payload, you can call this function to compute the corresponding checksum. For example:

source_port = 1234
dest_port = 5678
length = 10
payload = b'Hello'
checksum = calculate_checksum(source_port, dest_port, length, payload)
print('Checksum:', checksum)