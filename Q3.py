import asyncio
import struct


async def send_packet(writer, src_port, dst_port, payload):
    # calculate checksum
    length = len(payload) + 8
    checksum = compute_checksum(src_port, dst_port, length, payload)

    # pack packet header and payload into bytes
    packet = struct.pack('<HHHH', src_port, dst_port, length, checksum) + payload.encode()

    # send packet to server
    writer.write(packet)
    await writer.drain()


async def recv_and_decode_packet(reader):
    # receive packet from server
    packet = await reader.read(1024)

    # unpack packet header and payload
    src_port, dst_port, length, checksum = struct.unpack('<HHHH', packet[:8])
    payload = packet[8:].decode()

    # verify checksum
    if compute_checksum(src_port, dst_port, length, payload) != 0:
        raise ValueError("Invalid checksum")

    # print decoded packet information
    print(f"Base64: {packet.encode('base64').decode().strip()}")
    print(f"Server Sent: {packet}")
    print("Decoded Packet:")
    print(f"Source Port: {src_port}")
    print(f"Dest Port: {dst_port}")
    print(f"Data Length: {length}")
    print(f"Checksum: {checksum}")
    print(f"Payload: {payload}")
    print("")


async def request_utc_time(reader, writer):
    while True:
        # send UDP packet with payload '1111'
        await send_packet(writer, 0, 542, '1111')

        # receive and decode response
        await recv_and_decode_packet(reader)

        # wait for 1 second before repeating
        await asyncio.sleep(1)


def compute_checksum(src_port, dst_port, length, payload):
    # pack fields into bytes
    src_bytes = src_port.to_bytes(2, byteorder='little')
    dst_bytes = dst_port.to_bytes(2, byteorder='little')
    length_bytes = length.to_bytes(2, byteorder='little')
    payload_bytes = payload.encode()

    # concatenate bytes in correct order
    message = src_bytes + dst_bytes + length_bytes + b'\x00\x00' + payload_bytes

    # calculate one's complement sum
    checksum = 0
    for i in range(0, len(message), 2):
        word = message[i] + (message[i + 1] << 8)
        checksum += word
        checksum = (checksum & 0xffff) + (checksum >> 16)

    # return one's complement
    return ~checksum & 0xffff


async def main():
    reader, writer = await asyncio.open_connection('127.0.0.1', 8888)
    await recv_and_decode_packet(reader)  # receive and decode hello message
    await request_utc_time(reader, writer)  # send and receive UDP packets


if _name_ == '_main_':
    asyncio.run(main())