import unittest
from my_module import calculate_checksum

class TestCalculateChecksum(unittest.TestCase):

    def test_calculate_checksum(self):
        # Test with a payload that is an even number of bytes
        source_port = 1234
        dest_port = 5678
        length = 6
        payload = b'Hello!'
        expected_checksum = 49112
        self.assertEqual(calculate_checksum(source_port, dest_port, length, payload), expected_checksum)

        # Test with a payload that is an odd number of bytes
        source_port = 4321
        dest_port = 8765
        length = 5
        payload = b'1234'
        expected_checksum = 56494
        self.assertEqual(calculate_checksum(source_port, dest_port, length, payload), expected_checksum)

        # Test with a payload that is a single byte
        source_port = 1111
        dest_port = 2222
        length = 1
        payload = b'\x00'
        expected_checksum = 49537
        self.assertEqual(calculate_checksum(source_port, dest_port, length, payload), expected_checksum)

if __name__ == '__main__':
    unittest.main()
